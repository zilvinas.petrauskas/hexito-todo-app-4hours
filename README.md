## Task description and requirements
Create a TODO list application. A simplified Trello version.
Main page should have a default column ("Todo") with initial todo items that are loaded
from mock API. Main page should have a single search input.

* Todo item should have a title, description and a date.
* Initial todo items are loaded from mocked API.
* New todo items are not saved and will not be loaded.
* Design only desktop version.
* A single search input for todo item filtering.
* Try to allocate no more than 4 hours to complete this task. Everything unfinished can be documented in readme section **Improvements**

## User abilities
* User can add new columns
* User can drag and drop todo items from one column to another.
* User can delete column or delete a task
* User can search(filter) tasks by title and description

## Bonus
* If you create a dockerized development environment.
* If you configure and use firebase to persist and load todo items and columns.
* If you write at least one unit test.
* If you leave comments on how and what would you improve if you had more time.

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

## Improvements
- add todo onclick without a button (drag and click at the same time issue)
- like in trello - focus next input when clickign "Add"
- get name of list and display it in popup (prop via vuex)
- put search input into search component
- change route to children on dialog (todo edit popup) show up so it could be possible to share the link directly to the issue
- display date in popup or near todo item in list
- not a requirement - but draggable columns (todo lists)
- add firebase to save data
- finish test for component with vuetify
- clean up code in general
- add more style - was the last thign I cared about, went all in on functionality first


... tested app a bit more

- close new todo form after it loses focus
- delete column (add a button on the right) and delete all the todos in it
- rename column (just like in todo dialog when clicking on title - it turns into input)
- add [x] on the right on search input to clear it
