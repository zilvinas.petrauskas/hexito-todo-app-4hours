
export function calculateDragPosition(initial, prev, next){
    let position = initial;
    if(prev && next){
        position = (prev.position + next.position) / 2;
    }else if(prev){
        position = prev.position + (prev.position / 2);
    }else if(next){
        position = next.position / 2;
    }
    return position;
}

export function sortByPosition(a,b){
    if(a.position > b.position){
        return 1;
    }else if(a.position < b.position){
        return -1;
    }
    return 0;
}