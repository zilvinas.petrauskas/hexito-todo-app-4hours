import { sortByPosition } from '@/utils'

const initialTodos = [
    {
        id: 1,
        title: 'Todo #1',
        description: 'sadk fhasdl fjhasd',
        date: 6546354354,
        list: 1,
        position: 0,
    },
    {
        id: 2,
        title: 'Todo #2',
        description: '',
        date: 7894654654,
        list: 1,
        position: 50000,
    },
    {
        id: 3,
        title: 'Todo #3',
        description: '33333333',
        date: 5254254352435,
        list: 1,
        position: 100000,
    }
]

const TodoObj = (todo, state) => {
    
    const now = Date.now();

    const cleanTodo =  {
        id: now,
        title: 'Todo '+now,
        description: '',
        date: now,
        list: 1,
        position: getLargestPosition(state) + 100000,
    }
    return {
        ...cleanTodo,
        ...todo,
    }
}

function getLargestPosition(state){
    return state.todos.reduce((acc, val)=> {
        if(val.position > acc){
            acc = val.position;
        }
    }, 0);
}

const todosModule = {
    state: ()=>({
        todos: initialTodos || [],
    }),
    getters:{
        todos(state){
            return state.todos;
        },
        sortedTodos(state){
            return state.todos.sort(sortByPosition);
        }
    },
    mutations:{
        addTodo(state, newTodo){
            state.todos.push(TodoObj(newTodo, state));
        },
        updateTodo(state, { todoId, updates }){
            let idx = state.todos.findIndex(t => t.id == todoId);
            if(idx > -1){
                let updatedTodo = {
                    ...state.todos[idx]
                }
                Object.entries(updates).forEach(([key, value])=>{
                    updatedTodo[key] = value;
                })
                state.todos.splice(idx, 1, updatedTodo);
            }
        },
        deleteTodo(state, todoId){
            state.todos = state.todos.filter(t => t.id != todoId);
        },
    },
    actions:{
        addTodo({commit}, newTodo){
            commit('addTodo', newTodo);
        },
        updateTodo({commit}, { todoId, updates }){
            commit('updateTodo', { todoId, updates });
        },
        deleteTodo({commit}, todoId){
            commit('deleteTodo', todoId);
        }
    }
}

export default todosModule;