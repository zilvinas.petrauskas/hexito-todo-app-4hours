const searchModule = {
    state: ()=>({
        search: ''
    }),
    getters:{
        search(state){
            return state.search;
        },
    },
    mutations:{
        updateSearch(state, newSearch){
            state.search = newSearch;
        },
    },
    actions:{
        updateSearch({commit}, newSearch){
            commit('updateSearch', newSearch);
        },
    }
}

export default searchModule;