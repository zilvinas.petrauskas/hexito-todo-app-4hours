const initialList = [
    {
        id: 1,
        title: 'Todo',
    },
    // {
    //     id: 2,
    //     title: 'In progress',
    // },
]

const todoListsModule = {
    state: ()=>({
        todoList: initialList || [],
    }),
    getters:{
        allLists(state){
            return state.todoList;
        }
    },
    mutations:{
        addList(state, newList){
            state.todoList.push({
                ...newList,
                id: Date.now(),
            });
        }
    },
    actions:{
        addList({commit}, newList){
            commit('addList', newList);
        }
    }
}

export default todoListsModule;