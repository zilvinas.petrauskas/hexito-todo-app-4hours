import { calculateDragPosition } from '@/utils';


test("should return same position",async()=>{

    const position = calculateDragPosition(100, null, null);
    expect(position).toBe(100);
})

test("should return largest position",async()=>{

    /*
        0 -> 50
        1 -> 100  #1 goes to #3 (initial 100, prev 300, next null) expect 450?
        2 -> 200
        3 -> 300
    */

    const position = calculateDragPosition(100, {position: 300}, null);
    expect(position).toBe(450);
})

test("should return lower position",async()=>{

    /*
        0 -> 50
        1 -> 100  
        2 -> 200
        3 -> 300 #3 goes to #0 (300 init, null prev, 50 next) expect 25?
    */

    const position = calculateDragPosition(300, null, {position: 50});
    expect(position).toBe(25);
})

test("should return mid position",async()=>{

    /*
        0 -> 50
        1 -> 100  #1 goes to #2 (init 100, prev 200, next 300) expect 250
        2 -> 200
        3 -> 300 
    */

    const position = calculateDragPosition(100, {position: 200}, {position: 300});
    expect(position).toBe(250);
})